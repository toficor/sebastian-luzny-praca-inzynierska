﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIntensityChanger : MonoBehaviour
{
    private Light _light;
    private float _timePassed;
    private float _startIntensity;
    private float _newIntensity;

    private void Awake()
    {
        _light = GetComponent<Light>();
        _startIntensity = _light.intensity;
        _newIntensity = _startIntensity + 0.5f;
    }

    private void Update()
    {
        _timePassed += Time.deltaTime;
        _light.intensity = Mathf.Lerp( _startIntensity, _newIntensity, Mathf
            .PingPong( _timePassed, 0.5f ) );
    }
}
