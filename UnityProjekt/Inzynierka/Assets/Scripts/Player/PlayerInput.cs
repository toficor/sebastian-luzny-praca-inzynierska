﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private int playerId;

    private Player myPlayer;

    public float HorizontalAxis => myPlayer.GetAxis( "Horizontal" );
    public float VerticalAxis => myPlayer.GetAxis( "Vertical" );
    public bool Jump => myPlayer.GetButton( "Jump" );
    public bool Attack => myPlayer.GetButton( "Attack" );
    

    public Player Player
    {
        get
        {
            return myPlayer;
        }
    }

    private void Awake()
    {
        myPlayer = ReInput.players.GetPlayer( playerId );
    }

   


}
