﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private string tagToAffect;
    [SerializeField] private IAttacker iAttacker;
    [SerializeField] private bool isProjectile;
    [SerializeField] private bool isObstacle;

    private void OnEnable()
    {
        iAttacker = GetComponentInParent<IAttacker>();
    }

    private void OnTriggerEnter( Collider other )
    {
        DoDamage( other );
    }

    public void DoDamage( Collider other )
    {
        if ( other.CompareTag( tagToAffect ) == false )
        {
            return;
        }

        IHealth health = other.GetComponent<IHealth>();

        if ( health != null )
        {
            if ( isProjectile )
            {
                health.GetDamage( damage );
                Destroy( gameObject );
            }
            else
            {
                if ( isObstacle == false )
                {
                    if ( iAttacker.IsAttacking )
                    {
                        health.GetDamage( damage );
                    }
                }
                else
                {
                    health.GetDamage( damage );
                }
            }
        }
    }
}
