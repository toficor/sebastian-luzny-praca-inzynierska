﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimatorController : MonoBehaviour
{
    [SerializeField] private Animator myAnimator;

    public bool IsAttackingCheck()
    {
        return myAnimator.GetCurrentAnimatorStateInfo( 0 ).IsName( "Attacking" );
    }
}
