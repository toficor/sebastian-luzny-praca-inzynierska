﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class PlayerController : MonoBehaviour, IAttacker
{
    [SerializeField] private PlayerAnimatorController myAnimatorController;
    [SerializeField] private PlayerInput myInput;
    [SerializeField] private ThirdPersonCharacter myThirdPersonCharacter;
    [SerializeField] private PlayerHealth myHealth;
    [SerializeField] private CheckpointManager checkpointManager;

    private Vector3 move;
    private Vector3 camForward;

    private bool jump;
    private bool attack;
    private bool isAttacking;
    private bool canBeTeleported;

    private float teleportTimer;
    public PlayerAnimatorController MyAnimatorController
    {
        get
        {
            return myAnimatorController;
        }
    }

    public bool CanBeTeleported
    {
        get
        {
            return canBeTeleported;
        }
    }

    public ThirdPersonCharacter MyThirdPersonCharacter
    {
        get
        {
            return myThirdPersonCharacter;
        }
    }

    public Vector3 PlayerPosition
    {
        get
        {
            return transform.position;
        }
    }

    public PlayerHealth PlayerHealth
    {
        get
        {
            return myHealth;
        }
    }

    public bool IsAttacking
    {
        get
        {
            return isAttacking;
        }

        set
        {
            isAttacking = value;
        }

    }

    private void Awake()
    {
        PlayerHealth.onGetKilled += ResetPlayer;
    }

    private void OnEnable()
    {

    }

    private void ResetPlayer()
    {
        transform.position = checkpointManager.currentCheckpoint.position;
        PlayerHealth.ResurectPlayer();
    }

    private void TeleportAvability()
    {
    
    }

    private void Update()
    {
        jump = myInput.Jump;
        attack = myInput.Attack;
        myHealth.ManageAliveState();
    }

    private void FixedUpdate()
    {
        HandleMotion();
    }

    private void HandleMotion()
    {
        float h = myInput.HorizontalAxis;
        float v = myInput.VerticalAxis;

        move = v * Vector3.forward + h * Vector3.right;

        myThirdPersonCharacter.Move( move, false, jump, attack );

        jump = false;
        attack = false;
    }

    public void SetAttackingState( int state )
    {
        isAttacking = Convert.ToBoolean( state );
    }
}
