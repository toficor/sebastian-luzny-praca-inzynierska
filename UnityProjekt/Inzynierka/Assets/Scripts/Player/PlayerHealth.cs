﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerHealth : MonoBehaviour, IHealth
{
    [SerializeField] private int maxHealthPoints;
    [SerializeField] private bool isAlive;

    public Action<int> onGetDamage;
    public Action<int> onHeal;
    public Action onGetKilled;
    
    private int currentHealthPoints;

    private void Awake()
    {
        currentHealthPoints = maxHealthPoints;
        onHeal += HealDamage;
        isAlive = true;
    }

    public int MaxHealth
    {
        get
        {
            return maxHealthPoints;
        }
        set
        {
            maxHealthPoints = value;
        }
    }

    public int CurrentHealth
    {
        get
        {
            return currentHealthPoints;
        }
        set
        {
            currentHealthPoints = value;
        }
    }

    public void GetDamage( int damage )
    {
        currentHealthPoints -= damage;
        onGetDamage?.Invoke(damage);
    }

    private void HealDamage( int amount )
    {
        currentHealthPoints += amount;
    }

    public void Kill()
    {
        onGetKilled?.Invoke();     
    }

    public void ResurectPlayer()
    {
        currentHealthPoints = maxHealthPoints;
    }

    public void ManageAliveState()
    {
        if ( CurrentHealth <= 0 )
        {
            isAlive = false;
            Kill();
        }
        else
        {
            isAlive = true;
        }
    }
}
