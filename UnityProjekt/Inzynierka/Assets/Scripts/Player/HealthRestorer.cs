﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthRestorer : MonoBehaviour
{
    [SerializeField] private int healAmount = 4;
    [SerializeField] private PlayerHealth playerHealth;



    private void OnTriggerEnter( Collider other )
    {
        playerHealth?.onHeal?.Invoke( healAmount );
        gameObject.SetActive( false );
    }
}
