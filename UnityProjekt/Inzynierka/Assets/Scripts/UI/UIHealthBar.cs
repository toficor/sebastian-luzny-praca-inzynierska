﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHealthBar : MonoBehaviour
{
    [SerializeField] private PlayerHealth playerHealth;
    [SerializeField] private List<GameObject> hearts = new List<GameObject>();
    [SerializeField] private GameObject heartPrefab;
    [SerializeField] private Transform heartsParent;

    private void Awake()
    {
        playerHealth.onGetDamage += OnGetDamage;
        playerHealth.onGetKilled += OnGetKilled;
        playerHealth.onHeal += OnGetHealed;
    }

    private void OnGetDamage( int damage )
    {
        for ( int i = 0; i < damage; i++ )
        {
            if ( i > hearts.Count )
            {
                break;
            }
            Destroy( hearts[ i ] );
            hearts.RemoveAt( i );
        }
    }

    private void OnGetHealed( int amount )
    {
        if ( playerHealth.CurrentHealth >= playerHealth.MaxHealth )
        {
            return;
        }

        for ( int i = 0; i < amount; i++ )
        {
            var heart = Instantiate( heartPrefab, heartsParent );
            heart.SetActive( true );

            hearts.Add( heart );

        }
    }

    private void OnGetKilled()
    {
        for ( int i = 0; i < playerHealth.MaxHealth; i++ )
        {
            hearts.Add( Instantiate( heartPrefab, heartsParent ) );
            hearts[ i ].SetActive( true );
        }
    }
}
