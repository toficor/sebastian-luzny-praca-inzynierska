﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonShootState : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    private AIDragon aiDragon;
    override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( aiDragon == null )
        {
            aiDragon = animator.GetComponentInParent<AIDragon>();
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( aiDragon != null )
        {
            aiDragon.ManageAttackTimmer();
            aiDragon.LookAtPlayer();

            if ( aiDragon.ReadyToAttack )
            {

                aiDragon.HandleShooot();                

                aiDragon.SetParameter( "IsShooting", false );
                aiDragon.SetParameter( "IsThinking", true );
            }

            if ( aiDragon.IsPlayerInAttackRange == false )
            {
                aiDragon.SetParameter( "IsShooting", false );
                aiDragon.SetParameter( "IsFlying", true );
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
