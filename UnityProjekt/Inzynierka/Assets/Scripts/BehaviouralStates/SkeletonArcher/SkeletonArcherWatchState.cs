﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonArcherWatchState : StateMachineBehaviour
{
    private AISkeletonArcher myAicontroller;
 

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( myAicontroller == null )
        {
            myAicontroller = animator.GetComponentInParent<AISkeletonArcher>();
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( myAicontroller != null )
        {

            if ( myAicontroller.IsPlayerVisible )
            {
                myAicontroller.SetParameter( "isAttacking", true );
                myAicontroller.SetParameter( "isWatching", false );
            }
            else
            {
                myAicontroller.HandleTurn();
            }

            if ( myAicontroller.PlayerIsNear )
            {
                myAicontroller.SetParameter( "isRunningAway", true );
                myAicontroller.SetParameter( "isWatching", false );
                myAicontroller.SetParameter( "isAttacking", false );
                myAicontroller.SetParameter( "isConfused", false );
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
