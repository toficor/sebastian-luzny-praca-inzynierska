﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonWarriorPatrolState : StateMachineBehaviour
{
    private AISkeletonWarrior myaiController;
    private Animator myMotionAnimator;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        myaiController = animator.GetComponentInParent<AISkeletonWarrior>();

        if ( myaiController != null )
        {
            myaiController.GotoNextPoint();
            myaiController.SetAgentSpeed( 1.25f );
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        if ( myaiController != null )
        {
            if ( myaiController.IsPlayerVisible() )
            {                
                myaiController.SetParameter("isPatroling", false);
                myaiController.SetParameter( "isChasing", true );
            }

            if ( myaiController.ShouldChangePatrolPoint() )
            {
                myaiController.GotoNextPoint();                
            }
        }
    }



    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
