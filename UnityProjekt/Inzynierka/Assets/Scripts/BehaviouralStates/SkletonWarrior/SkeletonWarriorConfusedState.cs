﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonWarriorConfusedState : StateMachineBehaviour
{

    public float confusionDuration = 3f;

    private AISkeletonWarrior myAiController;
    private float confusionTimer;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        myAiController = animator.GetComponentInParent<AISkeletonWarrior>();
        confusionTimer = 0f;

        if ( myAiController != null )
        {
            myAiController.ManageNavMeshAgentMotion(false);
        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate( Animator animator, AnimatorStateInfo stateInfo, int layerIndex )
    {
        confusionTimer += Time.deltaTime;

        if ( myAiController != null )
        {
            if ( confusionTimer >= confusionDuration )
            {
                myAiController.SetParameter( "isConfused", false );
                myAiController.SetParameter( "isPatroling", true );
            }

            if ( myAiController.IsPlayerVisible() )
            {
                myAiController.SetParameter( "isConfused", false );
                myAiController.SetParameter( "isChasing", true );
            }
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
