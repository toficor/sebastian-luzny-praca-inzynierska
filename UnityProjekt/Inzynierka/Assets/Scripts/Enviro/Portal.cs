﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( BoxCollider ) )]
public class Portal : MonoBehaviour
{
    [SerializeField] private Transform targetLocation;    

    private void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject.CompareTag( "Player" )  )
        {
            other.gameObject.transform.position = targetLocation.position;
            PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
            if ( playerController != null )
            {
                playerController.MyThirdPersonCharacter.m_Rigidbody.velocity = Vector3.zero;
            }
        }
    }
}
