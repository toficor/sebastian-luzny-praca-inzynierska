﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private void OnCollisionEnter( Collision collision )
    {
        if ( collision.collider.gameObject.CompareTag( "Player" ) )
        {
            collision.transform.parent = this.transform;

        }
    }


    private void OnCollisionStay( Collision collision )
    {
        if ( collision.collider.gameObject.CompareTag( "Player" ) )
        {
            PlayerController playerController = collision.collider.GetComponent<PlayerController>();
            playerController.MyThirdPersonCharacter.m_IsGrounded = true;
            playerController.MyThirdPersonCharacter.m_Rigidbody.velocity = new Vector3( playerController.MyThirdPersonCharacter.m_Rigidbody.velocity.x, 0f, playerController.MyThirdPersonCharacter.m_Rigidbody.velocity.z );
            collision.transform.lossyScale.Set( 1f, 1f, 1f );
        }
    }
    private void OnCollisionExit( Collision collision )
    {
        if ( collision.collider.gameObject.CompareTag( "Player" ) )
        {
            
            collision.transform.parent = null;
            collision.transform.lossyScale.Set( 1f, 1f, 1f );            
        }
    }
}
