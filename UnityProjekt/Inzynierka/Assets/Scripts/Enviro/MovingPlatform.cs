﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [Space]
    [SerializeField] private Transform platformTransform;
    [SerializeField] private Transform firstWaypoint;
    [SerializeField] private Transform secondWaypoint;
    [Space]
    [SerializeField] private AnimationCurve motionCurve;
    [Space]
    [SerializeField] private bool isHorizontal = true;


    private Transform currentTarget;
    private float progress;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawCube( firstWaypoint.position, new Vector3( 0.2f, 0.2f, 0.2f ) );
        Gizmos.DrawCube( secondWaypoint.position, new Vector3( 0.2f, 0.2f, 0.2f ) );
    }

    private void OnEnable()
    {
        currentTarget = firstWaypoint;
    }

    private void Update()
    {
        HandleMotion();
    }

    private void HandleMotion()
    {
        progress += Time.deltaTime;

        platformTransform.position = Vector3.Lerp( platformTransform.position, currentTarget.position, motionCurve.Evaluate( progress ) * 0.01f * speed );

        if ( isHorizontal )
        {

            if ( Mathf.Abs( platformTransform.position.x - currentTarget.position.x ) <= 0.1f )
            {
                currentTarget = currentTarget == firstWaypoint ? secondWaypoint : firstWaypoint;
                progress = 0f;
            }
        }
        else
        {
            //Debug.LogError( platformTransform.position.y - currentTarget.position.y );
            if ( Mathf.Abs( platformTransform.position.y - currentTarget.position.y) <= 0.3f )
            {
                currentTarget = currentTarget == firstWaypoint ? secondWaypoint : firstWaypoint;
                progress = 0f;
            }
        }

    }
 
}
