﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeObstacle : MonoBehaviour
{
    [SerializeField] private DamageDealer myDamageDealer;
    [SerializeField] private Vector3 forceVector = Vector3.up;
    [SerializeField] private float forceValue = 3f;

    private void OnCollisionEnter( Collision collision )
    {
        myDamageDealer.DoDamage( collision.collider );
        var playerRigidbody = collision.gameObject.GetComponent<Rigidbody>();
        AddForceToPlayer( playerRigidbody );
    }

    private void AddForceToPlayer( Rigidbody playerRigidbody )
    {
        playerRigidbody.AddForce( forceVector * forceValue, ForceMode.Impulse );
    }


}
