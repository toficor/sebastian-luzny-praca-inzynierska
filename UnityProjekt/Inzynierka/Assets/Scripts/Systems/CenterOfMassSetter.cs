﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOfMassSetter : MonoBehaviour
{

    [SerializeField] private Rigidbody myRigidbody;
    [SerializeField] private Transform myCenterOfMass;

    private void OnEnable()
    {
        myRigidbody.centerOfMass = myCenterOfMass.position;
    }
}
