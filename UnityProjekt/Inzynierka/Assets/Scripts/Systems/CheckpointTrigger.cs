﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointTrigger : MonoBehaviour
{

    [SerializeField] private CheckpointManager checkpointManager;
    private void OnTriggerEnter( Collider other )
    {
        checkpointManager.SaveCheckpoint( transform );
    }
}
