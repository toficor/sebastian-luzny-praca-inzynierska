﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    [SerializeField] private PostProcessVolume postProcessVolume;

    private Vignette vignette;

    private IEnumerator COR_VigneteFade()
    {
        postProcessVolume.profile.TryGetSettings( out vignette );

        while ( vignette.intensity.value < 1 )
        {
            
            vignette.intensity.value += Time.deltaTime;

            yield return null;
        }

        SceneManager.LoadScene( "Level", LoadSceneMode.Single );
    }

    public void LoadLevel()
    {
        StartCoroutine( COR_VigneteFade() );
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

}
