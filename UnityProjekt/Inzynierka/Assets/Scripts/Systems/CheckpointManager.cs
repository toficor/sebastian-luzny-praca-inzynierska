﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/CheckpointManager" )]
public class CheckpointManager : ScriptableObject
{
    public Transform currentCheckpoint;

    public void SaveCheckpoint(Transform checkpoint)
    {
        currentCheckpoint = checkpoint;
    }
}
