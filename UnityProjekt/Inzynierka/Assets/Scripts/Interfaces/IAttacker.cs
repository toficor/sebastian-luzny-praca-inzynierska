﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAttacker
{
    void SetAttackingState( int state );

    bool IsAttacking
    {
        get;
        set;
    }
}
