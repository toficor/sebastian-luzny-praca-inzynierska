﻿using Pathfinding;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDragon : AIBase
{
    public AIDestinationSetter aIDestinationSetter;
    public Animator animator;
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Transform projectileSpawnPosition;
    [SerializeField] private float distanceToAttack = 6f;
    [SerializeField] private float delayBetweenAttacks = 2f;
    [SerializeField] private float thinkingTime = 3f;
    [SerializeField] private float shotPower = 4f;
    [SerializeField] private GameObject projectile;

    private float attackTimer;
    private float thinkingTimer;

    private void Awake()
    {
        attackTimer = delayBetweenAttacks;
    }    

    public float AttackTimer
    {
        set
        {
            attackTimer = value;
        }
    }

    public float ThinkingTimer
    {
        set
        {
            thinkingTimer = value;
        }
    }

    public Transform PlayerTransform
    {
        get
        {
            return playerTransform;
        }
    }

    public bool IsPlayerInAttackRange
    {
        get
        {
            return Vector3.Distance( transform.position, playerTransform.position ) <= distanceToAttack;
        }
    }

    public bool ReadyToAttack
    {
        get
        {
            return attackTimer >= delayBetweenAttacks;
        }
    }

    public bool IsThinking
    {
        get
        {
            return thinkingTimer <= thinkingTime;
        }
    }

    public void ManageThinkingTimer()
    {
        thinkingTimer += Time.deltaTime;
    }

    public void ManageAttackTimmer()
    {
        attackTimer += Time.deltaTime;
    }

    public void HandleShooot()
    {
        Rigidbody arrowRigidbody = Instantiate( projectile, projectileSpawnPosition.position, transform.rotation ).GetComponent<Rigidbody>();
        Vector3 direction = ( playerTransform.position - transform.position + (Vector3.up * 2f) ).normalized;
        arrowRigidbody.AddForce( direction * shotPower, ForceMode.Impulse );
        attackTimer = 0f;
    }

    public float GetNextAction
    {
        get
        {
            return Random.Range( 0f, 1f );
        }
    }

    public void LookAtPlayer()
    {
        transform.LookAt( playerTransform );
    }

}
