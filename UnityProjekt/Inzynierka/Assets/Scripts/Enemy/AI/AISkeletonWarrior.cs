﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AISkeletonWarrior : AIBase, IAttacker
{
    [SerializeField] private NavMeshAgent myNavMeshAgent;
    [SerializeField] private AICharacterControl aICharacterControl;
    [SerializeField] private List<Transform> patrolPoints = new List<Transform>();
    [SerializeField] private float distanceToChangePatrolPoint = 0.5f;
    [SerializeField] private float distanceToAttack = 1f;
    [SerializeField] private float timeBetweenAttacks = 1f;
    [SerializeField] private Animator motionAnimator;
    [SerializeField] private LayerMask layerMask;

    private int destPointIndex = 0;
    private float attackTimer = 0;

    public bool isPatroling;
    public bool isChasing;
    public bool isAttacking;
    public bool isConfused;

    private float turnAmount;
    private float forwardAmount;
    private CapsuleCollider myCollider;
    private PlayerController targetPlayer;

    public bool IsAttacking
    {
        get => isAttacking;
        set => isAttacking = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        myNavMeshAgent.autoBraking = false;
        myCollider = GetComponent<CapsuleCollider>();
        isPatroling = true;
    }

    private void Update()
    {
        attackTimer += Time.deltaTime;

        if ( attackTimer >= timeBetweenAttacks && PlayerInAttackRange() )
        {
            attackTimer = 0f;
            aICharacterControl.IsAttacking = true;
        }
        else
        {
            aICharacterControl.IsAttacking = false;
        }

        
    }

    public bool ShouldChangePatrolPoint()
    {
        return !myNavMeshAgent.pathPending && myNavMeshAgent.remainingDistance <= distanceToChangePatrolPoint;
    }

    public void GotoNextPoint()
    {
        if ( patrolPoints.Count == 0 )
        {
            return;
        }

        if ( myNavMeshAgent.isStopped )
        {
            myNavMeshAgent.isStopped = false;
        }

        myNavMeshAgent.destination = patrolPoints[ destPointIndex ].position;
        destPointIndex = ( destPointIndex + 1 ) % patrolPoints.Count;
    }

    public void SetAgentSpeed( float speed )
    {
        myNavMeshAgent.speed = speed;
    }

    public void SetPlayerAsDestination()
    {
        myNavMeshAgent.destination = targetPlayer.transform.position;
    }

    public void ManageNavMeshAgentMotion(bool isStoped)
    {
        myNavMeshAgent.isStopped = isStoped;
    }

    public bool PlayerInAttackRange()
    {
        if ( targetPlayer == null )
        {
            return false;
        }

        return Vector3.Distance( gameObject.transform.position, targetPlayer.transform.position ) <= distanceToAttack;
    }


    public bool IsPlayerVisible()
    {
        RaycastHit hitInfo;
        Vector3 castOrigin = new Vector3( myCollider.center.x, myCollider.center.y, myCollider.center.z + myCollider.radius );

        Debug.DrawRay( transform.position + castOrigin, gameObject.transform.forward, Color.red );

        if ( Physics.SphereCast( transform.position + castOrigin, 0.5f, gameObject.transform.forward, out hitInfo, 10f ) )
        {
            if ( hitInfo.collider.gameObject.CompareTag( "Player" ) )
            {
                if ( targetPlayer == null )
                {
                    targetPlayer = hitInfo.collider.gameObject.GetComponent<PlayerController>();
                }
                return true;
            }
        }

        return false;

    }

    public void SetAttackingState( int state )
    {
        isAttacking = Convert.ToBoolean( state );
    }
}
