﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBase : MonoBehaviour
{
    [SerializeField] protected Animator myBehaviouralTree;

    public void ChangeState( string stateName )
    {
        myBehaviouralTree.Play( stateName );
    }

    public void SetParameter( string parameterName, bool parameterValue )
    {
        myBehaviouralTree.SetBool( parameterName, parameterValue );
    }

    public void SetParameter( string parameterName, float parameterValue )
    {
        myBehaviouralTree.SetFloat( parameterName, parameterValue );
    }

    public void SetParameter( string parameterName, int parameterValue )
    {
        myBehaviouralTree.SetInteger( parameterName, parameterValue );
    }

    public void SetParameter( string parameterName )
    {
        myBehaviouralTree.SetTrigger( parameterName );
    }
} 
