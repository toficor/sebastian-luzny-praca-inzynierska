﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class AISkeletonArcher : AIBase
{
    [SerializeField] private float oneSideWatchTime = 3f;
    [SerializeField] private float confusedTime = 10f;
    [SerializeField] private float distanceToRunAway = 3f;
    [SerializeField] private float shotPower;
    [SerializeField] private AICharacterControl aICharacterControl;
    [SerializeField] private Transform backFacingSpot;
    [SerializeField] private PlayerController playerController;

    [SerializeField] private GameObject arrowInHandModel;
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform projectileSpawnPosition;


    private bool isPlayerVisible;
    private bool wasPlayerSeen;
    private float turningTimer = 0f;
    private float confusionTimer = 0f;
    private PlayerController targetPlayerController;

    public bool IsPlayerVisible
    {
        set
        {
            isPlayerVisible = value;
        }

        get
        {
            return isPlayerVisible;
        }
    }

    public AICharacterControl myAiCharacterControll
    {
        get
        {
            return aICharacterControl;
        }
    }

    public bool WasPlayerSeen
    {
        set
        {
            wasPlayerSeen = value;
        }

        get
        {
            return wasPlayerSeen;
        }
    }

    public bool PlayerIsNear
    {
        get
        {
            return Vector3.Distance( transform.position, playerController.PlayerPosition ) <= distanceToRunAway && isPlayerVisible;
        }
    }

    public PlayerController TargetPlayerController
    {
        set
        {
            targetPlayerController = value;
        }
    }

    //public float ConfusedTime
    //{
    //    get
    //    {
    //        return confusedTime;
    //    }
    //}

    private void OnEnable()
    {
        DisableArrowInHand();
    }

    private Vector3 debugEscapeVector;
    private void OnDrawGizmos()
    {
        if ( debugEscapeVector != Vector3.zero )
        {

            Gizmos.DrawCube( debugEscapeVector, new Vector3( 0.3f, 0.3f, 0.3f ) );

        }
    }

    public void HandleTurn()
    {
        turningTimer += Time.deltaTime;

        if ( turningTimer >= oneSideWatchTime )
        {
            turningTimer = 0;
            DoTurn();
        }
    }

    public void DisableArrowInHand()
    {
        arrowInHandModel.SetActive( false );
    }

    public void EnableArrowInHand()
    {
        arrowInHandModel.SetActive( true );
    }

    private void DoTurn()
    {
        aICharacterControl.agent.SetDestination( backFacingSpot.position );
    }

    public void HandleConfusion()
    {
        aICharacterControl.IsAttacking = false;
        confusionTimer += Time.deltaTime;
        if ( confusionTimer >= confusedTime )
        {
            SetParameter( "isConfused", false );
            SetParameter( "isWatching", true );
            confusionTimer = 0f;
        }
    }
    public void HandleAttack()
    {
        aICharacterControl.IsAttacking = true;
    }

    public void HandleRunAway()
    {
        
        RunAway();

    }
    public void HandleShot()
    {

        if ( targetPlayerController == null )
        {
            return;
        }

        Rigidbody arrowRigidbody = Instantiate( projectilePrefab, projectileSpawnPosition.position, transform.rotation ).GetComponent<Rigidbody>();
        Vector3 direction = ( targetPlayerController.transform.position - transform.position + Vector3.up ).normalized;


        arrowRigidbody.AddForce( direction * shotPower, ForceMode.Impulse );
    }

    private void RunAway()
    {
        aICharacterControl.IsAttacking = false;
        aICharacterControl.agent.isStopped = false;

        Vector3 disToPlayer = transform.position - playerController.transform.position;
        Vector3 newPosition = transform.position + disToPlayer;

        aICharacterControl.agent.SetDestination( newPosition );
        
    }

    private void Update()
    {

    }
}
