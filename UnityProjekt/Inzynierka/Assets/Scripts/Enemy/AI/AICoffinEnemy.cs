﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AICoffinEnemy : AIBase
{
    [SerializeField] private float attackDelay = 5f;
    [SerializeField] private int attackDamage = 1;
    [SerializeField] private Transform raycastSource;
    [SerializeField] private PlayerController playerController;
    [SerializeField] private Animator skeletonAnimator;

    private float attackTimer = 0;
    private bool isAttacking;
    private RaycastHit raycastHit;

    private void Awake()
    {
        attackTimer = attackDelay;
    }

    public bool IsAttacking
    {
        set
        {
            isAttacking = value;
        }
    }

    public Animator SkeletonAnimator
    {
        get
        {
            return skeletonAnimator;
        }
    }

    private void Update()
    {
        attackTimer += Time.deltaTime;
    }

    public bool PlayerVisible()
    {
        Debug.DrawRay( raycastSource.position, raycastSource.forward, Color.red );
        if ( Physics.Raycast( raycastSource.position, raycastSource.forward, out raycastHit ) )
        {
            if ( raycastHit.collider.CompareTag( "Player" ) )
            {
                return true;
            }
        }
        return false;
    }

    public void DoDamageToPlayer()
    {

        playerController.PlayerHealth.GetDamage( attackDamage );
        attackTimer = 0;

    }

    public bool AbleToAttack()
    {
        return attackTimer >= attackDelay;
    }
}
