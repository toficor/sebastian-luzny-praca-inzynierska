﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour, IHealth
{
    [SerializeField] private int maxHealthPoints;
    [SerializeField] private ParticleSystem getHitParticle;
    [SerializeField] private GameObject killParticle;

    [Header( "Shaking" )]
    [SerializeField] private bool shakeOnGetHit;
    [SerializeField] private Vector2 shakeRange = new Vector2( -0.5f, 0.5f );
    [SerializeField] private int amountOfShakes = 2;


    private int currentHealthPoints;
    private Vector3 originPosition;

    private void Awake()
    {
        currentHealthPoints = maxHealthPoints;
    }

    private void OnEnable()
    {
        originPosition = gameObject.transform.position;
    }

    private void Update()
    {
        ManageLifeState();
    }

    public int MaxHealth
    {
        get
        {
            return maxHealthPoints;
        }
        set
        {
            maxHealthPoints = value;
        }
    }
    public int CurrentHealth
    {
        get
        {
            return currentHealthPoints;
        }
        set
        {
            currentHealthPoints = value;
        }
    }

    public void GetDamage( int damage )
    {
        getHitParticle.Play( true );
        currentHealthPoints -= damage;
        if ( shakeOnGetHit )
        {
            DoShake();
        }
    }

    public void ManageLifeState()
    {
        if ( currentHealthPoints <= 0 )
        {
            Kill();
        }
    }

    public void Kill()
    {
        Instantiate( killParticle, transform.position,Quaternion.identity );
        Destroy( gameObject );
    }

    private void DoShake()
    {
        StartCoroutine( COR_DoShake() );
    }

    private IEnumerator COR_DoShake()
    {
        for ( int i = 0; i < amountOfShakes; i++ )
        {
            gameObject.transform.position = new Vector3( i % 2 == 0 ? gameObject.transform.position.x + .1f : gameObject.transform.position.x - .1f, gameObject.transform.position.y, gameObject.transform.position.z );
            yield return null;
        }

        gameObject.transform.position = originPosition;
    }
}
