﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eyeHack : MonoBehaviour
{
    // Start is called before the first frame update
    private PlayerController playerController;
    private AISkeletonArcher aiSkeletonArcher;
    private Vector3 initialPosition;
    private Quaternion initRotation;
    bool once;

    private void Awake()
    {
        playerController = FindObjectOfType<PlayerController>();
        aiSkeletonArcher = GetComponentInParent<AISkeletonArcher>();

    }

    private void OnEnable()
    {
        initialPosition = gameObject.transform.localPosition;
        initRotation = gameObject.transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {

        if ( playerController != null && aiSkeletonArcher.myAiCharacterControll.IsAttacking )
        {
            once = true;
            transform.LookAt( playerController.transform );
            return;
        }

        if ( aiSkeletonArcher.myAiCharacterControll.IsAttacking == false && once )
        {
            gameObject.transform.localRotation = initRotation;
            gameObject.transform.localPosition = initialPosition;
            once = false;
        }
    }
}
