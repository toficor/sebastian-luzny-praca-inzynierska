﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{
    void GetDamage(int damage);
    void Kill();

    int MaxHealth
    {
        get;
        set;
    }

    int CurrentHealth
    {
        get;
        set;
    }
}
