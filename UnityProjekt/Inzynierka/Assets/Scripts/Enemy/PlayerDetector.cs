﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{

    [SerializeField] private AISkeletonArcher myAiController;
    [SerializeField] private Transform rayOrgin;
    [SerializeField] private LayerMask layersToIgnore;

    private void OnEnable()
    {

    }
    private void OnTriggerStay( Collider other )
    {
        if ( other.CompareTag( "Player" ) )
        {
            CheckPlayerVisibility( other.transform );
        }
    }

    private void OnTriggerEnter( Collider other )
    {
        if ( other.CompareTag( "Player" ) )
        {
            CheckPlayerVisibility( other.transform );
        }
    }

    private void OnTriggerExit( Collider other )
    {
        if ( other.CompareTag( "Player" ) )
        {
            CheckPlayerVisibility( other.transform );
        }
    }


    private void CheckPlayerVisibility( Transform playerTransfomr )
    {

        RaycastHit hit;

        Vector3 rayDirection = playerTransfomr.position - rayOrgin.position + Vector3.up;

        Debug.DrawRay( rayOrgin.position, rayDirection, Color.red );

        if ( Physics.Raycast( rayOrgin.position, rayDirection, out hit, 100f, ~layersToIgnore ) )
        {
            if ( hit.collider.gameObject.CompareTag( "Player" ) )
            {

                myAiController.IsPlayerVisible = true;
                myAiController.TargetPlayerController = hit.collider.GetComponent<PlayerController>();
            }
            else
            {
                myAiController.IsPlayerVisible = false;
                myAiController.TargetPlayerController = null;
            }
        }

    }


}
